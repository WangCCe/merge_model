import torch
from torch.utils import data
import h5py


class subDataset(data.Dataset):
    """Characterizes a dataset for PyTorch"""

    def __init__(self, list_IDs, path=None):
        """Initialization"""
        self.list_IDs = list_IDs
        self.file_path = path
        self.dataset = None

    def __len__(self):
        """Denotes the total number of samples"""
        return len(self.list_IDs)

    def __getitem__(self, index):
        """Generates one sample of data"""
        # Select sample
        ID = self.list_IDs[index]
        if self.dataset is None:
            self.dataset = h5py.File(self.file_path, 'r')
#         print(ID)
        X = self.dataset[ID][()]
        X = torch.from_numpy(X)

        return X.unsqueeze(dim=0)
    
class Dataset(data.Dataset):
    """Characterizes a dataset for PyTorch"""

    def __init__(self, list_IDs, labels, path1,path2):
        """Initialization"""
        self.labels = labels
        self.list_IDs = list_IDs
        self.dataset1 = subDataset(list_IDs, path=path1)
        self.dataset2 = subDataset(list_IDs, path=path2)
    def __len__(self):
        """Denotes the total number of samples"""
        return len(self.list_IDs)

    def __getitem__(self, index):
        """Generates one sample of data"""
        # Select sample
        ID = self.list_IDs[index]
        exc = self.dataset1[index]
        vocal = self.dataset2[index]
        y = self.labels[ID]

        return torch.cat((exc,vocal)), y

