import numpy as np
import torch
import os
from torchtracer import Tracer


def save_checkpoint(model, optimizer, path, string):
    torch.save(model.state_dict(), os.path.join(path, 'model_{}.pth'.format(string)))
    torch.save(optimizer.state_dict(), os.path.join(path, 'optimizer_{}.pth'.format(string)))
