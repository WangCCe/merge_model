#!/usr/bin/env python
# coding: utf-8

# In[1]:


import warnings
from collections import Iterable

import torch.nn.functional as F
from torch.utils.checkpoint import checkpoint

from uawa_att import *

use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")

# In[2]:
dic_stride = {1: 1, 3: 1, 5: 1, 7: 1, 9: 2, 11: 3, 13: 4, 15: 4}


def multi(nums, t):
    res = 1
    for i in range(t):
        res *= nums[i]
    return res


def cal_field(fs, ss):
    n = len(fs)
    res = [1] * (n + 1)
    for i in range(1, n + 1):
        res[i] = res[i - 1] + (fs[i - 1] - 1) * multi(ss, i - 1)
    return res


def get_reception_field(ks):
    if ks is None:
        return
    ks = np.array(ks, dtype='int')
    ks = ks.reshape(ks.shape[0], ks.shape[2])
    fs = []
    ss = []
    for l in range(ks.shape[0]):
        fs.append(ks[l][0])
        fs.append(2)
        ss.append(dic_stride[ks[l][0]])  # stride
        ss.append(2)
    return cal_field(fs, ss)[-1]


class ModuleWrapperIgnores2ndArg(nn.Module):
    def __init__(self, module):
        super().__init__()
        self.module = module

    def forward(self, x, dummy_arg=None):
        assert dummy_arg is not None
        x = self.module(x)
        return x


class SingleScaleModel(nn.Module):
    def __init__(self, time_only=None, kernel_size_list=None, use_lstm=False, dropout=0.0, dilation=None,
                 freq_range=None, ckpt=False, keep_origin=False):
        super(SingleScaleModel, self).__init__()
        self.ckpt = ckpt
        self.keep_origin = keep_origin
        if self.keep_origin:
            self.in_channels = 2
        else:
            self.in_channels = 1
        if self.ckpt:
            self.dummy_tensor = torch.ones(1, dtype=torch.float32, requires_grad=True).to(device)
            momentum = sqrt(0.1)
        else:
            momentum = 0.1
        self.freq_range = freq_range or 128  # default 128

        if dilation is None:
            dilation = [[1, 1]] * len(kernel_size_list)  # default no dilation
        if kernel_size_list is None:
            kernel_size_list = [[5, 5], [5, 5], [3, 3], [3, 3]]  # assume this is the baseline

        self.layers = list()
        self.half = lambda x: x // 2 + 1
        self.padding_list = []
        self.stride_list = []
        for i in range(len(kernel_size_list)):
            if isinstance(kernel_size_list[i][0], list):
                kernel_size_list[i] = kernel_size_list[i][0]  # to deal with [[[3,3],[[3,3]]]
            padding = tuple(map(lambda x, y: x // 2 + (x - 1) * (y - 1) // 2, kernel_size_list[i],
                                dilation[i]))
            stride = tuple(map(lambda x: dic_stride[x], kernel_size_list[i]))
            layer = BasicConv2d(max(self.in_channels, i * 16), (i + 1) * 16, kernel_size=tuple(kernel_size_list[i]),
                                padding=padding,
                                stride=stride, momentum=momentum, dilation=dilation[i])
            self.layers.append(layer)
            self.layers.append(nn.MaxPool2d(2, padding=1))
            self.padding_list.append(padding)
            self.stride_list.append(stride)
        self.cnns = nn.Sequential(*self.layers)
        if self.ckpt:
            self.module_wrapper = ModuleWrapperIgnores2ndArg(self.cnns)
        self.kernel_size_list = kernel_size_list
        self.dilation = dilation
        self.use_lstm = use_lstm
        self.dropout = nn.Dropout(dropout)
        if use_lstm:
            self.hidden_size = 128
            input_dim = self.cal_freq_dim()
            input_dim = input_dim * len(self.kernel_size_list) * 16
            self.lstm = nn.LSTM(input_dim, self.hidden_size, batch_first=True, bidirectional=True)
            self.fc = nn.Linear(self.hidden_size * 2, 4)
        else:
            self.GAP = nn.AdaptiveAvgPool2d(1)  # todo: make cnn not use zero-pad part
            self.fc = nn.Linear(len(self.kernel_size_list) * 16, 4)

    def cal_freq_dim(self):
        x = self.freq_range
        for i in range(len(self.kernel_size_list)):
            x = (x + 2 * self.padding_list[i][0] - self.dilation[i][0] * (self.kernel_size_list[i][0] - 1) - 1) // \
                self.stride_list[i][0] + 1
            x = self.half(x)
        return x

    def cal_time_dim(self, x):
        for i in range(len(self.kernel_size_list)):
            x = (x + 2 * self.padding_list[i][1] - self.dilation[i][1] * (self.kernel_size_list[i][1] - 1) - 1) // \
                self.stride_list[i][1] + 1
            x = self.half(x)
        return x

    def forward(self, x, lengths):
        if self.keep_origin:
            out = x.view(x.shape[0], 2, x.shape[1], -1)
            lengths = lengths // 2
        else:
            out = x.view(x.shape[0], 1, x.shape[1], x.shape[2])
        if self.freq_range:
            out = out[:, :, :self.freq_range, :]  # crop the input
        # print(out.shape, out)
        lengths = self.cal_time_dim(lengths)
        if self.ckpt:
            out = checkpoint(self.module_wrapper, out, self.dummy_tensor)
        else:
            out = self.cnns(out)
        if self.use_lstm:
            out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
            out = out.transpose(1, 2).contiguous()
            # lengths = torch.max(lengths, torch.tensor(out.shape[1]).to(device)) wrong
            out = torch.nn.utils.rnn.pack_padded_sequence(out, lengths.cpu(), batch_first=True)
            out, _ = self.lstm(out)
            out, _ = torch.nn.utils.rnn.pad_packed_sequence(out, batch_first=True)
            # out = out[:, -1, :]
            out = out.contiguous().view(out.shape[0] * out.shape[1], -1)
            adjusted_lengths = torch.stack([i * lengths[0] + l - 1 for i, l in enumerate(lengths)])
            out = out.index_select(0, adjusted_lengths)
        else:
            out = self.GAP(out)
            out = out.view(out.shape[0], out.shape[1])
        out = self.dropout(out)
        out = self.fc(out)
        return out, 0


class BasicConv2d(nn.Module):

    def __init__(self, in_channels, out_channels, momentum, **kwargs):
        super(BasicConv2d, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, bias=False, **kwargs)
        self.bn = nn.BatchNorm2d(out_channels, momentum=momentum)

    #         self.bn = nn.BatchNorm2d(out_channels, eps=0.001)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)

        return F.relu(x, inplace=True)


class Nothing(nn.Module):
    def __init__(self):
        super(Nothing, self).__init__()
        pass

    def forward(self, x):
        return x


class MultiScaleBlock(nn.Module):
    def __init__(self, in_channels, out_channels, only_time=False, kernel_sizes=None, dilation=None, ckpt=False):
        super(MultiScaleBlock, self).__init__()
        # uncertain num of branches supported.
        if ckpt:
            momentum = sqrt(0.1)
        else:
            momentum = 0.1
        if kernel_sizes is not None:
            if isinstance(kernel_sizes, Iterable):
                assert len(kernel_sizes) > 0, "block should have at least one branch"
                if only_time:
                    warnings.warn('ignoring parameter: only_time')
                self.layers = nn.ModuleDict()
                for i in range(len(kernel_sizes)):
                    if dilation is None:
                        layer = BasicConv2d(in_channels, out_channels, kernel_size=tuple(kernel_sizes[i]),
                                            padding=tuple(map(lambda x: x // 2, kernel_sizes[i])),
                                            stride=tuple(map(lambda x: dic_stride[x], kernel_sizes[i])),
                                            momentum=momentum)
                    else:
                        layer = BasicConv2d(in_channels, out_channels, kernel_size=tuple(kernel_sizes[i]),
                                            padding=tuple(
                                                map(lambda x, y: x // 2 + (x - 1) * (y - 1) // 2, kernel_sizes[i],
                                                    dilation[i])),
                                            stride=tuple(map(lambda x: dic_stride[x], kernel_sizes[i])),
                                            momentum=momentum, dilation=dilation[i])
                    self.layers['layer_' + str(i)] = layer
            else:
                raise RuntimeError('kernel sizes should be iterable')
        else:
            #  old version, fixed kernel sizes
            if only_time:
                self.conv1 = BasicConv2d(in_channels, out_channels, kernel_size=(1, 1), padding=0)
                self.conv2 = BasicConv2d(in_channels, out_channels, kernel_size=(3, 1), padding=(1, 0))
                self.conv3 = BasicConv2d(in_channels, out_channels, kernel_size=(5, 1), padding=(2, 0))
                for i in range(3):
                    layer = BasicConv2d(in_channels, out_channels, kernel_size=(i * 2 + 1, 1), padding=(i, 0))
            else:
                self.conv1 = BasicConv2d(in_channels, out_channels, kernel_size=1, padding=0)
                self.conv2 = BasicConv2d(in_channels, out_channels, kernel_size=3, padding=1)
                self.conv3 = BasicConv2d(in_channels, out_channels, kernel_size=5, padding=2)
                for i in range(3):
                    layer = BasicConv2d(in_channels, out_channels, kernel_size=i * 2 + 1, padding=i)
            self.layers['layer_' + str(i)] = layer
        if len(self.layers) > 1:
            self.conv1_1 = BasicConv2d(out_channels * len(self.layers), out_channels, kernel_size=1)
        else:
            self.conv1_1 = Nothing()

    def forward(self, x):
        features = []
        for name, layer in self.layers.items():
            new_feature = layer(x)
            features.append(new_feature)
        features = torch.cat(features, 1)
        out = self.conv1_1(features)
        return out


class Baseline(nn.Module):
    def __init__(self, hidden_size=128):
        super(Baseline, self).__init__()
        # 输入[40,1,128,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,65,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,33,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,17,39]
        self.conv4 = nn.Sequential(
            nn.Conv2d(48, 64, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,9,20]
        #         self.lstm_fc=nn.Linear(20,1)
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(64 * 9, self.hidden_size, batch_first=True, bidirectional=True)
        self.dnn = nn.Linear(self.hidden_size * 2, 4)

    def forward(self, x):
        out = self.conv1(x)

        out = self.conv2(out)

        out = self.conv3(out)

        out = self.conv4(out)

        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        out, (hn, cn) = self.lstm(out)
        # 输出[40,20,128*2]
        # 取最后一维lstm输出
        out = out[:, -1, :]
        # 取平均
        #         out = torch.mean(out, dim=1,keepdim=True)
        # dnn加权
        #         out=self.lstm_fc(out.transpose(1,2))
        out = out.contiguous().view(out.size()[0], -1)

        out = self.dnn(out)
        return out


class Selfattention(nn.Module):
    def __init__(self, n_head, attn_units, hidden_size=128, dropout=0.0):
        super(Selfattention, self).__init__()
        # 输入[40,1,128,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,65,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,33,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,17,39]
        self.conv4 = nn.Sequential(
            nn.Conv2d(48, 64, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,9,20]
        self.hidden_size = hidden_size
        self.attn_units = attn_units
        self.n_head = n_head
        # LSTM(特征尺度，隐藏层），输入是[batch,时序，特征]，输出是[batch,len，hidden_size]
        self.lstm = nn.LSTM(64 * 9, self.hidden_size, batch_first=True, bidirectional=True)
        self.SelfAttention = SelfAttention(self.hidden_size, self.attn_units, self.n_head, dropout=0.0)
        self.dnn = nn.Linear(self.hidden_size * 2 * self.n_head, 4)

    def forward(self, x):
        out = self.conv1(x)
        out = self.conv2(out)
        out = self.conv3(out)
        # 把通道和特征维度相乘，LSTM的输入是（batch，时序，特征），之后交换特征和时序位置
        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        out, (hn, cn) = self.lstm(out)
        out, attn = self.SelfAttention(out)
        out = out.view(out.size()[0], -1)
        #         print(out.shape)
        #         out = out[:,-1,:]      #取最后一维lstm输出
        out = self.dnn(out)
        return out


class CRNN_length(nn.Module):
    def __init__(self, n_head, attn_units, hidden_size=128, dropout=0.0):
        super(CRNN_length, self).__init__()
        # 输入[40,1,384,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,193,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,97,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,49,39]
        self.hidden_size = hidden_size
        self.attn_units = attn_units
        self.n_head = n_head
        # LSTM(特征尺度，隐藏层），输入是[batch,时序，特征]，输出是[batch,len，hidden_size*2]
        self.lstm = nn.LSTM(48 * 49, self.hidden_size, batch_first=True, bidirectional=True)
        self.SelfAttention = SelfAttention(self.hidden_size, self.attn_units, self.n_head, dropout=0.0)
        self.dnn = nn.Linear(self.hidden_size * 2, 4)

    def forward(self, x, lengths):
        lengths = lengths.clone().detach()
        out = self.conv1(x)
        lengths = lengths // 2 + 1
        out = self.conv2(out)
        lengths = lengths // 2 + 1
        out = self.conv3(out)
        lengths = lengths // 2 + 1
        mask = (torch.arange(lengths[0]).cuda()[None, :] < lengths[:, None]).float()
        # [40,39]
        #         print(mask)
        #         print(mask.shape)
        # 把通道和特征维度相乘，LSTM的输入是（batch，时序，特征），之后交换特征和时序位置
        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        out = torch.nn.utils.rnn.pack_padded_sequence(out, lengths, batch_first=True)
        #         h0 = torch.randn(2, out.size()[0], self.hidden_size).cuda()       #也可以不随机初始化
        #         c0 = torch.randn(2, out.size()[0], self.hidden_size).cuda()
        out, (hn, cn) = self.lstm(out)
        out, _ = torch.nn.utils.rnn.pad_packed_sequence(out, batch_first=True)
        # [40,39,128*2]
        out, attn = self.SelfAttention(out, mask)
        out = out.view(out.size()[0], -1)
        #         print(out.shape)
        #         out = out[:,-1,:]      #取最后一维lstm输出
        out = self.dnn(out)
        return out


class Channelattention(nn.Module):
    def __init__(self, channel_pool, hidden_size=128):
        super(Channelattention, self).__init__()
        # 输入[40,1,128,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,65,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,33,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,17,39]
        self.conv4 = nn.Sequential(
            nn.Conv2d(48, 64, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,9,20]
        #         self.lstm_fc=nn.Linear(20,1)
        self.channel_pool = channel_pool
        self.ChannelAttention3 = ChannelAttention(64, self.channel_pool)
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(64 * 9, self.hidden_size, batch_first=True, bidirectional=True)
        self.dnn = nn.Linear(self.hidden_size * 2, 4)

    def forward(self, x):
        out = self.conv1(x)

        out = self.conv2(out)

        out = self.conv3(out)

        out = self.conv4(out)

        out_channel = out
        attn_channel, out = self.ChannelAttention3(out)

        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        out, (hn, cn) = self.lstm(out)
        # 输出[40,20,128*2]
        # 取最后一维lstm输出
        out = out[:, -1, :]
        # 取平均
        #         out = torch.mean(out, dim=1,keepdim=True)
        # dnn加权
        #         out=self.lstm_fc(out.transpose(1,2))
        out = out.contiguous().view(out.size()[0], -1)

        out = self.dnn(out)
        return out, out_channel, attn_channel


class CRNN_test(nn.Module):
    def __init__(self, hidden_size=128):
        super(CRNN_test, self).__init__()
        # 输入[40,1,128,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,65,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,33,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,17,39]
        self.conv4 = nn.Sequential(
            nn.Conv2d(48, 64, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,9,20]
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(64 * 9, self.hidden_size, batch_first=True, bidirectional=True)
        self.dnn = nn.Linear(self.hidden_size * 2, 4)

    def forward(self, x):
        out = self.conv1(x)

        out = self.conv2(out)

        out = self.conv3(out)

        out = self.conv4(out)

        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        out, (hn, cn) = self.lstm(out)

        out = out[:, -1, :]  # 取最后一维lstm输出

        out = out.contiguous().view(out.size()[0], -1)

        out = self.dnn(out)
        return out


class CRNN_ConvChannel(nn.Module):
    def __init__(self, n_head, attn_units, channel_pool, hidden_size=128, dropout=0.0):
        super(CRNN_ConvChannel, self).__init__()
        # 输入[40,1,384,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,193,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,97,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,49,38]
        # 自适应变长
        #         self.Adap=nn.AdaptiveAvgPool2d([49,20])
        self.conv4 = nn.Conv2d(48, 48, kernel_size=(1, 1))
        self.conv5 = nn.Conv2d(48, 48, kernel_size=(1, 1))
        # 通道注意力，将不同通道乘相应权重相加

        # pool可选avg/max/var/avg+max……
        self.channel_pool = channel_pool
        self.ChannelAttention3 = ChannelAttention(48, self.channel_pool)
        #         self.SpatialAttention=SpatialAttention(kernel_size=3)
        # 输出[40,48,49,39]
        self.hidden_size = hidden_size
        self.attn_units = attn_units
        self.n_head = n_head
        # LSTM(特征尺度，隐藏层），输入是[batch,时序，特征]，输出是[batch,len，hidden_size]
        #         self.lstm = nn.LSTM(49*1,self.hidden_size,batch_first=True,bidirectional=True)
        self.lstm = nn.LSTM(48 * 49, self.hidden_size, batch_first=True, bidirectional=True)
        #         self.SelfAttention=SelfAttention(self.hidden_size,self.attn_units,self.n_head,dropout=0.0)
        #         self.dnn = nn.Linear(self.hidden_size*2*self.n_head,4)
        self.dnn = nn.Linear(self.hidden_size * 2, 4)

    def forward(self, x):
        out = self.conv1(x)
        #         out1,attn_channel = self.ChannelAttention1(out)
        #         out=out1+out

        out = self.conv2(out)
        #         out1,attn_channel= self.ChannelAttention2(out)
        #         out=out1+out

        out = self.conv3(out)
        out1 = self.conv4(out)
        out2 = self.conv5(out)
        attn_out = torch.matmul(out1.transpose(3, 2), out2)

        attn_channel = self.ChannelAttention3(attn_out)
        out = torch.mul(attn_channel, out) + out
        ##是否进行残差
        #         out=out1+out
        #         print(out.shape)
        #         out=self.SpatialAttention(out)
        #         print(out.shape)
        #         out2 = self.conv4(out)
        #         out = out1 + out2
        # 把通道和特征维度相乘，LSTM的输入是（batch，时序，特征），之后交换特征和时序位置
        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        #         h0 = torch.randn(2, out.size()[0], self.hidden_size).cuda()       #也可以不随机初始化
        #         c0 = torch.randn(2, out.size()[0], self.hidden_size).cuda()
        out, (hn, cn) = self.lstm(out)
        out = out[:, -1, :]  # 取最后一维lstm输出
        #         out, attn=self.SelfAttention(out)
        out = out.contiguous().view(out.size()[0], -1)
        #         print(out.shape)

        out = self.dnn(out)
        return out


class CNN_GRU(nn.Module):
    def __init__(self, n_head, attn_units, hidden_size=128, dropout=0.0):
        super(CNN_GRU, self).__init__()
        # 输入[40,1,384,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,193,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,97,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,49,39]
        self.hidden_size = hidden_size
        self.attn_units = attn_units
        self.n_head = n_head
        # LSTM(特征尺度，隐藏层），输入是[batch,时序，特征]，输出是[batch,len，hidden_size]
        self.gru = nn.GRU(48 * 49, self.hidden_size, batch_first=True, bidirectional=True)
        self.SelfAttention = SelfAttention(self.hidden_size, self.attn_units, self.n_head, dropout=0.0)
        self.dnn = nn.Linear(self.hidden_size * 2 * self.n_head, 4)

    def forward(self, x):
        out = self.conv1(x)
        out = self.conv2(out)
        out = self.conv3(out)
        # 把通道和特征维度相乘，LSTM的输入是（batch，时序，特征），之后交换特征和时序位置
        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        h0 = torch.randn(2, out.size()[0], self.hidden_size).cuda()  # 也可以不随机初始化
        out, hn = self.gru(out, h0)
        out, attn = self.SelfAttention(out)
        out = out.view(out.size()[0], -1)
        #         print(out.shape)
        #         out = out[:,-1,:]      #取最后一维lstm输出
        out = self.dnn(out)
        return out


# In[3]:


class MTL(nn.Module):
    def __init__(self, n_head, attn_units, hidden_size=128, dropout=0.0):
        super(MTL, self).__init__()
        # 输入[40,1,384,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,193,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,97,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,49,39]
        self.hidden_size = hidden_size
        self.attn_units = attn_units
        # LSTM(特征尺度，隐藏层），输入是[batch,时序，特征]，输出是[batch,len，hidden_size]
        self.lstm = nn.LSTM(48 * 49, self.hidden_size, batch_first=True, bidirectional=True)
        self.SelfAttention = SelfAttention(self.hidden_size, self.attn_units, n_head, dropout=0.0)
        self.dnn_emo = nn.Linear(self.hidden_size * 2, 4)
        self.dnn_MTL = nn.Linear(self.hidden_size * 2, 2)

    def forward(self, x):
        out = self.conv1(x)
        out = self.conv2(out)
        out = self.conv3(out)
        # 把通道和特征维度相乘，LSTM的输入是（batch，时序，特征），之后交换特征和时序位置
        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        h0 = torch.randn(2, out.size()[0], self.hidden_size).cuda()  # 也可以不随机初始化
        c0 = torch.randn(2, out.size()[0], self.hidden_size).cuda()
        out, (hn, cn) = self.lstm(out, (h0, c0))
        out, attn = self.SelfAttention(out)
        out = out.view(out.size()[0], -1)
        #         out = out[:,-1,:]      #取最后一维lstm输出
        out_emo = self.dnn_emo(out)
        out_MTL = self.dnn_MTL(out)
        return out_emo, out_MTL

# In[ ]:
