import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F


class SoftmaxThenConv(nn.Module):
    def __init__(self):
        super(SoftmaxThenConv, self).__init__()
        self.cnn = nn.Conv1d(in_channels=2, out_channels=1,kernel_size=1, bias=False)

    def forward(self, x):
        x = F.softmax(x, dim=-1)
        return self.cnn(x).view(x.shape[0], -1)
