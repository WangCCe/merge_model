## 激励源声道模型融合  
选了单尺度搜索中性能最好的。 
### 运行  
1. get_output_of_best_model.ipynb，取四个模型的输出存起来。这里需要读取已保存的四个模型。   
2. merge_four_classifiers.ipynb  几种不需要训练新模型的融合方法。  
3. run.sh ， 需要训练新模型的融合方法。  
    run.sh中的几行可以分别运行。  
### 说明  
CNN加权分数的，loss过大而没有记录到merge.csv里，在运行日志（1.txt）里取了最高的UA。