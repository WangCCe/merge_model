#!/usr/bin/env python
# coding: utf-8


# In[ ]:


import argparse
import os
import sys
import time
import csv

parser = argparse.ArgumentParser()
parser.add_argument('--max_epoch', help='max epoch', type=int, dest='max_epoch', default=25)
parser.add_argument('--debug', help='debug or not', type=int, dest='debug', default=0)
parser.add_argument('--gamma', help='focal', type=float, dest='gamma', default=0)
parser.add_argument('--seed', help='random seed', type=int, dest='seed', default=2)  # default to 2
parser.add_argument('--decay', help='lr decay', type=int, dest='decay', default=0)  # default no decay
parser.add_argument('--outfile', help='where to save metrics', type=str, dest='out', default='merge.csv')
args = parser.parse_args()


gpu_visible = '0'  # changed since gpu1 is gone and 0 maps to real 0.
SEED = args.seed
os.environ["CUDA_VISIBLE_DEVICES"] = gpu_visible
import torch
torch.manual_seed(SEED)
torch.cuda.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
from torch.utils import data
import torch.nn as nn
import numpy as np
np.random.seed(SEED)
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
import pudb

import torchtracer
from torchtracer import Tracer
from visdom import Visdom
from utils import  save_checkpoint
from merge_dataset import Dataset
from merge_model import SoftmaxThenConv
from focalloss import *
# In[ ]:

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")

# In[ ]:
# kernel_size_list = [[[1,1],[1,3],[1,5]],[[1,1],[1,3],[1,5]],[[1,1],[1,3],[1,5]]]


DEBUG = args.debug
max_epochs = args.max_epoch
gamma = args.gamma
DECAY = args.decay
path_to_save_result = './'+args.out
if DEBUG:
    pudb.set_trace()


configs = {'batch_size': 64,
           'shuffle': True,
           'num_workers': 1}

lr = 0.0002

ALLDATA = False
CLIPGRAD = False

if DECAY:
    from torch.optim.lr_scheduler import MultiStepLR
    from torch.optim.lr_scheduler import CosineAnnealingLR




if gamma == 0:
    loss_func = nn.CrossEntropyLoss()
else:
    loss_func = FocalLoss(gamma=gamma)

dic = {'gamma': gamma, 'allData': ALLDATA, 'clipGrad': CLIPGRAD, 'SEED': SEED}
dic.update(vars(args))  # add args to dic

# In[ ]:


fold='4'
path_to_features='./best_model_outputs/{emo}.hdf5'
trainFileName = 'emoTrainImpro' + fold + '.txt'
testFileName = 'emoTestImpro' + fold + '.txt'
configs = {'batch_size': 64,
           'shuffle': False,
           'num_workers': 1}
labels = {}
dict_emo = {'a': 0, 'h': 1, 'n': 2, 's': 3}
with open('/home/wangce/' + trainFileName, 'r') as f:
    trainData = f.read().split('\n')
with open('/home/wangce/' + testFileName, 'r') as f:
    testData = f.read().split('\n')
total = trainData + testData

for item in total:
    labels[item] = dict_emo[item[4]]

training_set = Dataset(trainData, labels, path1=path_to_features.format(emo='exc'), path2=path_to_features.format(emo='vocal'))
training_generator = data.DataLoader(training_set, **configs)
validation_set = Dataset(testData, labels, path1=path_to_features.format(emo='exc'), path2=path_to_features.format(emo='vocal'))
validation_generator = data.DataLoader(validation_set, **configs)

if DEBUG:
    pudb.set_trace()
model = SoftmaxThenConv().to(device)
optim = torch.optim.Adam(model.parameters(), lr=lr)
dic['model'] = model
dic['optim'] = optim

print(sum(param.numel() for param in model.parameters()))
if DECAY:
    # scheduler = MultiStepLR(optim, milestones=[500, 700], gamma=0.3, last_epoch=-1)
    scheduler = CosineAnnealingLR(optim, T_max=20)
# In[ ]:

envPrefix = 'mergeModel_'


localTimeStrs = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
envName = envPrefix + localTimeStrs
best = {'ua': 0.0}

# In[ ]:

if not DEBUG:
    tracer = Tracer('/home/wangce/checkpoints/').attach(localTimeStrs)
    path_to_save_model_optim = '/home/wangce/checkpoints/'+localTimeStrs
    cfg = torchtracer.data.Config(dic)
    tracer.store(cfg)
    viz = Visdom(env=envName, port=8101)
    viz.text(str(dic), opts={'title': 'settings'})
    run_command = " ".join(sys.argv)
    viz.text(run_command, opts={'title': 'running command'})
    print(envName)
    tracer.log(msg=envName, file="envName")  # todo: put all msg to one log file
    tracer.log(msg=run_command, file="run_command")

# In[ ]:


t1 = time.time()
for epoch in range(1, max_epochs):
    Y_pred = torch.LongTensor().to(device)
    Y_true = torch.LongTensor().to(device)

    y_pred = torch.LongTensor().to(device)
    y_true = torch.LongTensor().to(device)

    trainBatchLoss = []
    testBatchLoss = []
    model.train()
    for local_batch, local_labels in training_generator:
        # Transfer to GPU
        local_batch, local_labels  = local_batch.to(device), local_labels.to(device), 

        # Model computations
        outputs = model(local_batch)
        optim.zero_grad()
        loss = loss_func(outputs, local_labels)
        loss.backward()
        ############
        if CLIPGRAD is True:
            nn.utils.clip_grad_value_(model.parameters(), 1)  # clip gradients
        optim.step()

        trainBatchLoss.append(loss.item())
        if DEBUG:
            pudb.set_trace()

    t2 = time.time()
    print('training model an epoch takes ' + str(round(t2 - t1, 2)) + 'seconds.')
    t1 = t2

    # Validation
    model.eval()
    with torch.set_grad_enabled(False):
        for local_batch, local_labels  in training_generator:
            local_batch, local_labels,  = local_batch.to(device), local_labels.to(device)

            outputs = model(local_batch)
            _, predict = torch.max(outputs, 1)
            Y_pred = torch.cat([Y_pred, predict], 0)
            Y_true = torch.cat([Y_true, local_labels], 0)
        for local_batch, local_labels, in validation_generator:
            # Transfer to GPU
            local_batch, local_labels = local_batch.to(device), local_labels.to(device)

            outputs = model(local_batch, )
            loss = loss_func(outputs, local_labels)
            testBatchLoss.append(loss.item())
            # print('testLoss: ', loss)
            _, predict = torch.max(outputs, 1)
            y_pred = torch.cat([y_pred, predict], 0)
            y_true = torch.cat([y_true, local_labels], 0)

    t2 = time.time()
    print('test model(training and testing set) takes ' + str(round(t2 - t1, 2)) + 'seconds.')
    t1 = t2

    #  减小lr
    if DECAY:
        scheduler.step()
    Confusion = confusion_matrix(Y_true.cpu(), Y_pred.cpu()).astype('float16').T
    confusion = confusion_matrix(y_true.cpu(), y_pred.cpu()).astype('float16').T
    Wa = precision_score(Y_true.cpu(), Y_pred.cpu(), average='micro')
    Ua = recall_score(Y_true.cpu(), Y_pred.cpu(), average='macro')
    wa = precision_score(y_true.cpu(), y_pred.cpu(), average='micro')
    ua = recall_score(y_true.cpu(), y_pred.cpu(), average='macro')
    # for load model and continue training, this should not take too much storage.
    if epoch % 100 == 0 and not DEBUG:
        save_checkpoint(model=model, optimizer=optim, path=path_to_save_model_optim, string=epoch)
    trainLoss = np.mean(trainBatchLoss)
    testLoss = np.mean(testBatchLoss)

    '''Confusion=torch.Tensor(Confusion)
    confusion=torch.Tensor(confusion)'''
    print(epoch)
    print('trainUA #{:.4f},trainWA #{:.4f}'.format(Ua, Wa))
    print(Confusion)
    print(Confusion.diagonal() / Confusion.sum(0))
    print('testUA #{:.4f},testWA #{:.4f}'.format(ua, wa))
    print(confusion)
    test_emotion_accs = confusion.diagonal() / confusion.sum(0)
    print(test_emotion_accs)
    if ua > float(best['ua']) and testLoss < 1.0:  # for lessen over fitting
        best['ua'] = '{:.4f}'.format(ua)
        best['wa'] = '{:.4f}'.format(wa)
        best['ang'] = test_emotion_accs[0]
        best['hap'] = test_emotion_accs[1]
        best['neu'] = test_emotion_accs[2]
        best['sad'] = test_emotion_accs[3]
        best['testloss'] = '{:.4f}'.format(testLoss)
        best['epoch'] = epoch
        best['visdom'] = envName
        if not DEBUG:
            save_checkpoint(model=model, optimizer=optim, path=path_to_save_model_optim, string='best')
    if not DEBUG:
        viz.line(X=np.column_stack(np.array([epoch, epoch])), Y=np.column_stack(np.array([trainLoss, testLoss])),
                 opts={'legend': ['trainLoss', "testLoss"], 'title': 'loss'}, win='loss', update='append')
        viz.line(X=np.column_stack(np.array([epoch, epoch, epoch, epoch])),
                 Y=np.column_stack(np.array([Wa, Ua, wa, ua])),
                 opts={'legend': ['trainWA', 'trianUA', 'testWA', 'testUA'], 'title': 'metrics'}, win='metrics',
                 update='append')
        viz.line(X=np.column_stack(np.array([epoch, epoch, epoch, epoch])),
                 Y=np.column_stack(np.array(test_emotion_accs)),
                 opts={'legend': ['test_ang', 'test_hap', 'test_neu', 'test_sad'], 'title': 'test_emotion_metrics'},
                 win='test_emotion_metrics', update='append')

        viz.save([envName])
        tracer.log(
            msg='Epoch #{:03d}\ttrain_loss: {:.4f}\ttest_loss: {:.4f}\ttrainWA: {:.4f}\ttrainUA: {:.4f}\ttestUA: {:.4f}\ttestUA: {:.4f}'.format(
                epoch, trainLoss, testLoss, Wa, Ua, wa, ua),
            file='losses')
        tracer.log(msg=str(epoch) + '\n' + str(Confusion) + '\n' + str(confusion) + '\n', file='confusion')
if not DEBUG:
    viz.text(str(best))
# write column name
if not os.path.exists(path_to_save_result):
    with open(path_to_save_result, 'a+', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter='\t')
        to_write = ['model']+list(best.keys())+list(dic.keys())
        writer.writerow(to_write)
with open(path_to_save_result, 'a+', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter='\t')
    to_write = [dic['model']]+list(best.values())+list(dic.values())
    writer.writerow(to_write)
