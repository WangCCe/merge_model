## 模型融合  
早期使用的4.2.1.1中的模型，选了四种情感召回率最佳的模型各一个，总计四个模型。  
2021-06-01选了4.2.1.4 二维矩形CNN中的模型，选的方法同上。  
### 运行  
1. get_output_of_best_model.ipynb，取四个模型的输出存起来。这里需要读取已保存的四个模型。  
2. valid_saved_output.ipynb  验证一下读出来的模型没有问题。  
3. merge_four_classifiers.ipynb  几种不需要训练新模型的融合方法。  
4. run.sh ， 需要训练新模型的融合方法。  
    run.sh中的几行可以分别运行。