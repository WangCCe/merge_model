import h5py
import torch
from torch.utils import data


class Dataset(data.Dataset):
    """Characterizes a dataset for PyTorch"""

    def __init__(self, list_IDs, labels, path, max_length):
        """
        Initialization
        list_IDs : list of sample names, used for getting one sample
        labels : dict, mapping sample name to label
        path : str, path to hdf5
        max_length : int ,max lengths of time frame
        """
        self.labels = labels
        self.list_IDs = list_IDs
        self.file_path = path
        self.max_length = max_length
        self.dataset = None

    def __len__(self):
        """Denotes the total number of samples"""
        return len(self.list_IDs)

    def __getitem__(self, index):
        """Generates one sample of data"""
        # Select sample
        ID = self.list_IDs[index]
        if self.dataset is None:
            self.dataset = h5py.File(self.file_path, 'r')
        X = self.dataset[ID][()]
        X = torch.from_numpy(X)
        # [frames, bins]
        if self.max_length:
            X = X[:, :self.max_length]
        y = self.labels[ID]
        z = X.shape[1]

        return X, y, z, ID
