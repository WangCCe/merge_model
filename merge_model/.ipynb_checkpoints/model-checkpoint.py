#!/usr/bin/env python
# coding: utf-8

# In[1]:


import warnings
from collections import Iterable

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.checkpoint import checkpoint



use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")

# In[2]:
# kernel size to stride
dic_stride = {1: 1, 3: 1, 5: 1, 7: 1, 9: 2, 11: 3, 13: 4, 15: 4}

# for reception field computation
def multi(nums, t):
    res = 1
    for i in range(t):
        res *= nums[i]
    return res


def cal_field(fs, ss):
    n = len(fs)
    res = [1] * (n + 1)
    for i in range(1, n + 1):
        res[i] = res[i - 1] + (fs[i - 1] - 1) * multi(ss, i - 1)
    return res


def get_reception_field(ks):
    if ks is None:
        return
    ks = np.array(ks, dtype='int')

    fs = []
    ss = []
    for l in range(ks.shape[0]):
        fs.append(ks[l][0])
        fs.append(2)
        ss.append(dic_stride[ks[l][0]])  # stride
        ss.append(2)
    return cal_field(fs, ss)[-1].item()


# used if saving gpu memory
class ModuleWrapperIgnores2ndArg(nn.Module):
    def __init__(self, module):
        super().__init__()
        self.module = module

    def forward(self, x, dummy_arg=None):
        assert dummy_arg is not None
        x = self.module(x)
        return x


class SingleScaleModel(nn.Module):
    def __init__(self, kernel_size_list=None, use_lstm=False, dropout=0.0, dilation=None,
                 freq_range=None, ckpt=False):
        super(SingleScaleModel, self).__init__()
        self.ckpt = ckpt
        if self.ckpt:  # save gpu memory
            self.dummy_tensor = torch.ones(1, dtype=torch.float32, requires_grad=True).to(device)
            momentum = sqrt(0.1)
        else:
            momentum = 0.1
        self.freq_range = freq_range or 128  # default 128

        if dilation is None:  # process none dilation
            dilation = [[1, 1]] * len(kernel_size_list)  # default no dilation
        if kernel_size_list is None:
            kernel_size_list = [[5], [5], [3], [3]]  # assume this is the baseline
        for kernel_size in kernel_size_list:
            kernel_size.insert(0, 1)  # [[1, 5], [1, 5], [1, 3], [1, 3]]
        self.layers = list()
        self.half = lambda x: x // 2 + 1
        self.padding_list = []
        self.stride_list = []
        for i in range(len(kernel_size_list)):
            # for same size of input and output
            padding = tuple(map(lambda x, y: x // 2 + (x - 1) * (y - 1) // 2, kernel_size_list[i],
                                dilation[i]))
            # big stride for big kernel size
            stride = tuple(map(lambda x: dic_stride[x], kernel_size_list[i]))
            layer = BasicConv2d(max(1, i * 16), (i + 1) * 16, kernel_size=tuple(kernel_size_list[i]),
                                padding=padding,
                                stride=stride, momentum=momentum, dilation=dilation[i])
            self.layers.append(layer)
            self.layers.append(nn.MaxPool2d(2, padding=1))
            self.padding_list.append(padding)
            self.stride_list.append(stride)
        self.cnns = nn.Sequential(*self.layers)
        if self.ckpt:
            self.module_wrapper = ModuleWrapperIgnores2ndArg(self.cnns)
        self.kernel_size_list = kernel_size_list
        self.dilation = dilation
        self.use_lstm = use_lstm
        self.dropout = nn.Dropout(dropout)
        if use_lstm:
            self.hidden_size = 128
            input_dim = self.cal_freq_dim()
            input_dim = input_dim * len(self.kernel_size_list) * 16
            self.lstm = nn.LSTM(input_dim, self.hidden_size, batch_first=True, bidirectional=True)
            self.fc = nn.Linear(self.hidden_size * 2, 4)
        else:
            self.fc = nn.Linear(len(self.kernel_size_list) * 16, 4)

    def cal_freq_dim(self):
        x = self.freq_range
        for i in range(len(self.kernel_size_list)):
            x = (x + 2 * self.padding_list[i][0] - self.dilation[i][0] * (self.kernel_size_list[i][0] - 1) - 1) // \
                self.stride_list[i][0] + 1
            x = self.half(x)
        return x

    def cal_time_dim(self, x):
        for i in range(len(self.kernel_size_list)):
            x = (x + 2 * self.padding_list[i][1] - self.dilation[i][1] * (self.kernel_size_list[i][1] - 1) - 1) // \
                self.stride_list[i][1] + 1
            x = self.half(x)
        return x

    def forward(self, x, lengths):
        out = x.view(x.shape[0], 1, x.shape[1], x.shape[2])
        if self.freq_range:
            out = out[:, :, :self.freq_range, :]  # drop high fre bins of input, used in chap 3 exc input.
        # print(out.shape, out)
        lengths = self.cal_time_dim(lengths)
        if self.ckpt:
            out = checkpoint(self.module_wrapper, out, self.dummy_tensor)
        else:
            out = self.cnns(out)
        if self.use_lstm:
            out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
            out = out.transpose(1, 2).contiguous()
            # lengths = torch.max(lengths, torch.tensor(out.shape[1]).to(device)) wrong
            out = torch.nn.utils.rnn.pack_padded_sequence(out, lengths.cpu(), batch_first=True)
            out, _ = self.lstm(out)
            out, _ = torch.nn.utils.rnn.pad_packed_sequence(out, batch_first=True)
            # out = out[:, -1, :]
            mask = (torch.arange(lengths[0]).cuda()[None, :] < lengths[:, None]).float()
            res = out * mask.unsqueeze(-1)  # res: (batch, lengths[0], n_channels)
            res = res.sum(dim=1)  # res:(batch, n_channels)
            out = res / (lengths.unsqueeze(-1))  # res:(batch, n_channels)
        else:
            out = torch.nn.functional.adaptive_avg_pool2d(out, (1, out.size()[3]))
            # 把通道和特征维度相乘，LSTM的输入是（batch，时序，特征），之后交换特征和时序位置
            out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
            out = out.transpose(1, 2).contiguous()
            mask = (torch.arange(lengths[0]).cuda()[None, :] < lengths[:, None]).float()
            res = out * mask.unsqueeze(-1)  # res: (batch, lengths[0], n_channels)
            res = res.sum(dim=1)  # res:(batch, n_channels)
            out = res / (lengths.unsqueeze(-1))  # res:(batch, n_channels)
        out = self.dropout(out)
        out = self.fc(out)
        return out, 0


class BasicConv2d(nn.Module):

    def __init__(self, in_channels, out_channels, momentum, **kwargs):
        super(BasicConv2d, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, bias=False, **kwargs)
        self.bn = nn.BatchNorm2d(out_channels, momentum=momentum)

    #         self.bn = nn.BatchNorm2d(out_channels, eps=0.001)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)

        return F.relu(x, inplace=True)
