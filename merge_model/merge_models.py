import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F


class SoftmaxThenFc(nn.Module):
    def __init__(self):
        super(SoftmaxThenFc, self).__init__()
        self.fc = nn.Linear(16, 4, bias=False)

    def forward(self, x):
        x = F.softmax(x, dim=-1)
        x = x.view(x.shape[0], -1)
        return self.fc(x)


class SoftmaxThenAvg(nn.Module):
    def __init__(self):
        super(SoftmaxThenAvg, self).__init__()
        pass

    def forward(self, x):
        x = F.softmax(x, dim=-1)
        return torch.mean(x, dim=1)


class Likelyhood(nn.Module):
    def __init__(self):
        super(Likelyhood, self).__init__()
        

    def forward(self, x):
        x = F.softmax(x, dim=-1)
        data, index = torch.topk(x, k=2,dim=-1)
        x = x / data[:,:,1:]
        
        return torch.diagonal(x,dim1=-2,dim2=-1)
    

class ModLikelyhood(nn.Module):
    def __init__(self):
        super(ModLikelyhood, self).__init__()
        

    def forward(self, x):
        x = F.softmax(x, dim=-1)
        mask = torch.eye(4).byte().cuda()
        scores = x.diagonal(dim1=-2,dim2=-1)
        masked_x = x.masked_fill(mask, -float('inf'))
        max_except_diag,index = masked_x.max(dim=-1)
        return scores/max_except_diag
    
    
class MostVotes(nn.Module):
    def __init__(self):
        super(MostVotes, self).__init__()
        pass
    def forward(self, x):
        x = F.softmax(x, dim=-1)
        scores, preds = torch.max(x, dim=-1)
        votes = torch.zeros(preds.shape[0],preds.shape[1]).cuda()
        for i in range(preds.shape[0]):
            for j in range(preds.shape[1]):
                votes[i][preds[i][j]]+=1
        for i in range(preds.shape[0]):
            scores[i].masked_fill_(~(votes[i]==torch.max(votes[i])),0)
       
        return scores

class SoftmaxThenConv(nn.Module):
    def __init__(self):
        super(SoftmaxThenConv, self).__init__()
        self.cnn = nn.Conv1d(in_channels=4, out_channels=1,kernel_size=1, bias=False)

    def forward(self, x):
        x = F.softmax(x, dim=-1)
        return self.cnn(x).view(x.shape[0], -1)
    
class LookAsDualClf(nn.Module):
    def __init__(self):
        super(LookAsDualClf, self).__init__()

    def forward(self, x):
        x = F.softmax(x, dim=-1)
        return torch.diagonal(x,dim1=-2,dim2=-1)