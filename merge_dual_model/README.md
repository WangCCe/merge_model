## 二分类模型融合  
训练二分类的代码见[这里](https://gitlab.com/bupt_pris_ser/baseline/-/tree/merge_dual_model_wangce)  
### 运行  
1. get_output_of_best_model.ipynb，取四个模型的输出存起来。这里需要读取已保存的四个模型。   
2. merge_four_classifiers.ipynb, 合并四个模型，按照正类的分数最大作为预测结果。  

### 说明  
2021 0604 跑出来UA0.687563330666779 WA0.6591760299625468，与之前跑出的略有不同。