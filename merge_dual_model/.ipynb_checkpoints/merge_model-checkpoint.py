import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F


class LookAsDualClf(nn.Module):
    def __init__(self):
        super(LookAsDualClf, self).__init__()

    def forward(self, x):
        x = F.softmax(x, dim=-1)
        return x[:,:,-1]